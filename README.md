# Ultimate Project Library (UPL)

Small project for an imaginary website that stores users and their associated projects, and provides a UI for an overview.
- **Express.js**: application server framework
- **EJS**: embedded JS templating language
- **Mongoose**: Node.js modeling library for MongoDB

Testing: 
- **Mocha**: executing the tests
- **Chai**: assertion library
- **Instanbul**: code coverage

![login](/uploads/df56c7445686314f621774b2fcdb2fdd/login.jpg)
![projects](/uploads/a0f9a75f68243746436cda0c542196b9/projects.jpg)
![user](/uploads/65c403f78780c6fa170678570b7f5c11/user.jpg)
